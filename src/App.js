import React from 'react';
import logo from './logo.svg';

import './App.scss';
import {CardList} from "./components/CardList"

function App() {
	
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />

				<CardList/>

				<a
					className="App-link"
					href="https://gitlab.com/learn-tech-projects/reactjs-practice-projects/promise-practice"
					target="_blank"
					rel="noopener noreferrer"
				>
					Project on github
				</a>				

									
			</header>
		</div>
	);
}

export default App;
