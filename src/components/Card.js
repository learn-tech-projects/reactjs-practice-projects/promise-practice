import React, { Component } from 'react';
import "./Card.scss"

export class Card extends Component {

	constructor(props){
		super(props)
	}

	render() {

		const {logo, image,title,text,link,} = this.props

		return (
			<div className="card">

				<div className="card__header">
				{ !image?(
					<p className="card__header-text">{title}</p>
				):(
					<div>
						
						<img src={image} alt="" className="card__header-banner"/>
						<img className="card__header-logo" src={logo} alt=""/>
					</div>
				
				)

				}
				</div>

				<div className="card__content">
					<p className="card__content-title">{title}</p>
					<p className="card__content-text">{text}</p>
					<a href={link} target="_blank" rel="noopener noreferrer"  className="card__content-link">{link}</a>
				</div>

			</div>
		);

	}
}
