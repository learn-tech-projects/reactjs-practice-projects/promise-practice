import React, { Component } from 'react';
import {Card} from "./Card";



export class CardList extends Component {
	constructor(props){
		super(props)

		this.state ={
			logo:"",
			image:"",
			title:"",
			text:"",
			link:"",
			cards:[
				{
					image:"https://codecondo.com/wp-content/uploads/2015/05/Resources-to-Get-You-Started-with-ReactJS.jpg" ,
					logo:"https://www.valuecoders.com/blog/wp-content/uploads/2016/08/react.png",
					title:"Social Card",
					text:"We’ll start off with an easy one. This is more of a component than a full-blown app, but it’s a good place to start.",
					link:"dev.to",
				},
			]

		}


		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleClick = this.handleClick.bind(this);

	}

	handleInputChange(event) {
		let {name,value} = event.target;
		
	    this.setState(
	    	{ [name] : value}
	    );
  	}

	handleClick(e){
		// console.log("Button worked")
		let newCard = {
			logo:this.state.logo,
			image:this.state.image,
			title:this.state.title,
			text:this.state.text,
			link:this.state.link,
		}

		let newCards = this.state.cards
		newCards.push(newCard)

		this.setState({
			cards:newCards
		})
	}

	render() {

		let cardsList = this.state.cards.map((card,index)=>
			<Card
				logo={card.logo}
				image={card.image} 
				title={card.title}
				text={card.text}
				link={card.link}
			/>		
		)		


		function addCard(card){

			let newCard = {
				logo:card.logo,
				image:card.image,
				title:card.title,
				text:card.text,
				link:card.link,
			}


		}

		return (
			<div className="card-list-body">

				<div className="card-inputs">
					<input type="text" name="image" value={this.state.image} placeholder="image" onChange={this.handleInputChange} />
					<input type="text" name="logo" value={this.state.logo} placeholder="logo" onChange={this.handleInputChange} />
					<input type="text" name="title" value={this.state.title} placeholder="title" onChange={this.handleInputChange} />
					<input type="text" name="text" value={this.state.text} placeholder="text" onChange={this.handleInputChange} />
					<input type="text" name="link" value={this.state.link} placeholder="link" onChange={this.handleInputChange} />

					<button onClick={this.handleClick}>Добавить карточку</button>
				</div>

				<div className="card-list">
					
					{cardsList}

				</div>				
			</div>
		);
	}
}
